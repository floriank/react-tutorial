const redux = require('redux')
const reactRedux = require('react-redux')

const SET_SEARCH_TERM = 'setSearchTerm'
const initialState = {
  searchTerm: ''
}

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SEARCH_TERM:
      return reduceSearchTerm(state, action)
    default:
      return state

  }
}

const reduceSearchTerm = (state, action) => {
  const newState = {}
  Object.assign(newState, state, {searchTerm: action.value})
  return newState
}

// For production
// const store = redux.createStore(rootReducer)
// For development, we use the redux devtools (boilerplate code from docs)
// Note: devtools requires an http-server running:
//    npm install -g http-server
//    http-server . -p 5050
const store = redux.createStore(rootReducer, initialState, redux.compose(
  typeof window === 'object' && typeof window.devToolsExtension !== 'undefined'
    ? window.devToolsExtension() : (f) => f
))

// state ... current state of the redux store
// Pull searchTerm out from the state into the props
// To be used as this.props.searchTerm within the component
const mapStateToProps = (state) => ({ searchTerm: state.searchTerm })

// To be used as this.props.setSearchTerm('house') within the component
const mapDispatchToProps = (dispatch) => ({
  setSearchTerm (searchTerm) {
    dispatch({type: SET_SEARCH_TERM, value: searchTerm})
  }
})

const connector = reactRedux.connect(mapStateToProps, mapDispatchToProps)

// Export rootReducer for testing
module.exports = { connector, store, rootReducer }
