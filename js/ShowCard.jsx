const React = require('react')
const { Link } = require('react-router')

const ShowCard = (props) => (
  <Link to={`/details/${props.imdbID}`}>
    <div className="show-card">
      <img src={`public/img/posters/${props.poster}`} className="show-card-img" />
      <div className="show-card-text">
        <h3 className="show-card-title">{props.title}</h3>
        <h4 className="show-card-year">{props.year}</h4>
        <p className="show-card-description">{props.description}</p>
      </div>
    </div>
  </Link>
)

// Type hinting for React, so component blows up if it does not get the right type, or nothing at all
// PropType validation will save you from bugs, i.e. good habit
// When shipping out to production, you could remote all PropTypes
const { string } = React.PropTypes

ShowCard.propTypes = {
  poster: string.isRequired,
  title: string.isRequired,
  description: string.isRequired,
  year: string.isRequired,
  imdbID: string.isRequired
}

module.exports = ShowCard
