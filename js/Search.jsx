const React = require('react')
const Header = require('./Header')
const ShowCard = require('./ShowCard')
const { object, string } = React.PropTypes
const { connector } = require('./Store')

// Note: could refactor into stateless component because it's not using the search anymore
const Search = React.createClass({
  propTypes: {
    route: object,
    searchTerm: string
  },
  render () {
    return (
      <div className="container">
        <Header showSearch />
        <div className="shows">
          {this.props.route.shows
            .filter((show) => `${show.title} ${show.description}`.toUpperCase().indexOf(this.props.searchTerm.toUpperCase()) >= 0)
            .map((show) => (
              // React feature: ES6 like spread operator {...show} maps everything inside show to attributes
              <ShowCard {...show} key={show.imdbID} />
          ))}
        </div>
      </div>
      )
  }
})

module.exports = connector(Search)
