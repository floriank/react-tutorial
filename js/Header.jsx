const React = require('react')
const { Link } = require('react-router')
const { func, bool, string } = React.PropTypes
const { connector } = require('./Store')

const Header = React.createClass({
  propTypes: {
    setSearchTerm: func,
    showSearch: bool,
    searchTerm: string
  },
  handleSearchTermEvent (e) {
    // So setSearchTerm comes from our store
    this.props.setSearchTerm(e.target.value)
  },
  render () {
    let utilSpace
    if (this.props.showSearch) {
      utilSpace = <input type="text" className="search-input" placeholder="search"
        value={this.props.searchTerm} onChange={this.handleSearchTermEvent} />
    } else {
      utilSpace = (
        <h2 className="header-back">
          <Link to="/search">
            Back
          </Link>
        </h2>
      )
    }
    return (
      <header className="header">
        <h1 className="brand">
          <Link to="/" className="brand-link">
            svideo
          </Link>
        </h1>
        {utilSpace}
      </header>
    )
  }
})

// Wraps it, connects it to our redux store, so you can do e.g.
// console.log(this.props.searchTerm)
// This is what redux does: it wraps the methods and state that we want and injects them into our props
module.exports = connector(Header)
